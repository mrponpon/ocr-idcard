FROM python:3.7-slim
RUN apt-get update -y
RUN apt-get install gcc -y
RUN apt-get install libsm6 libxext6 libxrender-dev libgl1-mesa-glx libglib2.0-0 libgtk2.0-dev -y

WORKDIR /ocr
ADD . /ocr

RUN pip install --no-cache-dir -r requirements.txt
RUN python -m pip install .

CMD uvicorn app:app --host "0.0.0.0" --port "5000" --workers 4 --reload --debug