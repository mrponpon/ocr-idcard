from fastapi import FastAPI, Depends, Header, HTTPException, Response
from fastapi.middleware.cors import CORSMiddleware
from object_detection import api_predict
import urllib3
urllib3.disable_warnings()
app = FastAPI(title = "OCR-IDCARD",version = "0.0.1",openapi_prefix="/ai/card")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)
@app.get("/api/v1/info")
async def information():
    return {"app_name": app.title , "version" : app.version}
    
app.include_router(
    api_predict.router,
    prefix="/api/v1",
    tags=["api_predict"],
    responses={404: {"message": "Not found"}},
)