# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
from starlette.responses import JSONResponse
from pydantic import BaseModel
from .alyn import deskew

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
from .utils import label_map_util
from .utils import visualization_utils as vis_util
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import json
import base64
from fastapi import FastAPI, File, UploadFile,APIRouter
# import pytesseract
from multiprocessing.pool import ThreadPool
import time
import datetime
import uuid
import logging
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import yaml
import operator
import imutils
import requests

# with open(f'{os.getcwd()}/object_detection/district.yaml',encoding='utf8') as file:
#     load_district = yaml.load(file, Loader=yaml.FullLoader)[0]
# with open(f'{os.getcwd()}/object_detection/province.yaml',encoding='utf8') as file:
#     load_province = yaml.load(file, Loader=yaml.FullLoader)[0]
# with open(f'{os.getcwd()}/object_detection/tambon.yaml',encoding='utf8') as file:
#     load_tambon = yaml.load(file, Loader=yaml.FullLoader)[0]

# logging.getLogger().setLevel(logging.INFO)
ip_topsarun_ocr = os.getenv("api_ocr")
manageai_key = os.getenv("manageai_key")

# Path to frozen detection graph .pb file, which contains the model that is used for object detection.
PATH_TO_CKPT = os.path.join('object_detection','inference_graph_mmy_idcard','frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join('object_detection','inference_graph_mmy_idcard','labelmap.pbtxt')

# Number of classes the object detector can identify
NUM_CLASSES = 12

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    init_op = tf.global_variables_initializer()

    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)
    
# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

router = APIRouter()
class Item(BaseModel):
    img: str

def sort_contours(cnts, method="left-to-right"):
    reverse = False
    i = 0
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))

    return (cnts, boundingBoxes)
def text_cosine(text_input,check_found):
    district = load_district['district'].copy()
    province = load_province['province'].copy()
    tambon = load_tambon['tambon'].copy()
    data_list = [district,province,tambon]
    idx = 0
    for data in data_list:
        data.insert(0,text_input)
        vec = TfidfVectorizer()
        X = vec.fit_transform(data)
        S = cosine_similarity(X[0:1],X)
        index, score = max(enumerate(S[0][1:]), key=operator.itemgetter(1))
        if score > 0:
            check_found[str(idx)] = True
            return data[1:][index],check_found
        idx += 1
    return text_input,check_found

def extract_address(img,value):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    sobel = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3)
    ret, binary = cv2.threshold(sobel, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)
    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 5))
    dilation = cv2.dilate(binary, element2, iterations=1)
    # cv2.imwrite(f'test/dilation_{value}.jpg',dilation)

    contours, _ = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    (contours, boundingBoxes) = sort_contours(contours,method ="left-to-right")
    _result = ''
    check_found = {'0':False,'1':False,'2':False}
    for i in range(1,len(contours)):
        cnt = contours[i]
        area = cv2.contourArea(cnt)
        if (area < 100):
            continue
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        x, y, w, h = cv2.boundingRect(cnt)

        height = abs(box[0][1] - box[2][1])
        width = abs(box[0][0] - box[2][0])

        if (height > width * 1.3):
            continue
        new_img = img[y:y+h, x:x+w]
        # text = tesseract(new_img,"tha",11)
        text = ocr_topsarun_api(new_img)

        try:
            text = text.replace("\n"," ").split(" ")
            text = ' '.join(text)
        except:
            pass
        text = re.sub(r"[-()\"#@;:*<>{}`+=~|!?,]", "", text)
        # text,check_found = text_cosine(text,check_found)
        # print(text)
        _result += f'{text} '
    return _result
    # if check_found['0'] and check_found['1'] and check_found['2']:
    #     return _result
    # else:
    #     # print(_result)
    #     return ""

def detect_text(img,value):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    sobel = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3)
    ret, binary = cv2.threshold(sobel, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)
    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 5))
    dilation = cv2.dilate(binary, element2, iterations=1)
    # cv2.imwrite(f'dilation_{value}.jpg',dilation)

    contours, _ = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    (contours, boundingBoxes) = sort_contours(contours,method ="right-to-left")
    for i in range(len(contours)):
        cnt = contours[i]
        area = cv2.contourArea(cnt)
        if (area < 100):
            continue
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        x, y, w, h = cv2.boundingRect(cnt)

        height = abs(box[0][1] - box[2][1])
        width = abs(box[0][0] - box[2][0])

        if (height > width * 1.3):
            continue
        new_img = img[y:y+h, x:x+w]
        return new_img

def ocr_topsarun_api(image):
    retval, buffer = cv2.imencode('.jpg', image)
    img_base64 = base64.b64encode(buffer).decode('utf-8')
    headers = {"manageai-key":str(manageai_key)}
    data = {
        "base64img": img_base64
    }
    r = requests.post(str(ip_topsarun_ocr), json = data,headers=headers,verify=False)
    result = r.json()
    if result !=None:
        return result["prediction"][0]["text"]
    else:
        return ""

def croping_img(image,ymin,ymax,xmin,xmax):
    return image[int(ymin):int(ymax), int(xmin)-8:int(xmax)+25]

def rotate_angle(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    dw = deskew.Deskew(
        input_numpy=image,
        output_numpy=True
    )
    rot_angle = dw.run()
    return rot_angle

def extract_text(image,key,value,im_height, im_width):
    ymin, xmin, ymax, xmax = key
    (xmin, xmax, ymin, ymax) = (xmin * im_width, xmax * im_width,
                    ymin * im_height, ymax * im_height)
    if value =='enfirstname' or value =='enlastname' or value =='religion' :
        crop_img = croping_img(image,ymin,ymax,xmin,xmax)
        crop_img = detect_text(crop_img,value)
    elif 'issuedate' in value or 'expiredate' in value:
        try:
            ymax=ymin+((ymax-ymin)/1.65)
            crop_img = croping_img(image,ymin,ymax,xmin,xmax)
        except Exception as e:
            print(e)
            crop_img = croping_img(image,ymin,ymax,xmin,xmax)
    else:
        crop_img = croping_img(image,ymin,ymax,xmin,xmax)
     
    # cv2.imwrite(f'test/{value}.jpg',crop_img)

    if value[:2] == 'en' or value == 'idnumber':
        text = ocr_topsarun_api(crop_img)
    elif value == 'address':
        text = extract_address(crop_img,value)
        return text

    else:
        text = ocr_topsarun_api(crop_img)
        if value == 'thname':           
            text = text.split(" ")
            if len(text) > 2:
                text = text[len(text)-2:]
            text = ' '.join(text)

    if value == 'enbirthdate' or value == 'thbirthdate':
        try:
            text = text.split(re.findall(r'(.*?)\d+.*?$', text)[0])[1]
        except Exception as e:
            print(e)

    text = re.sub(r"[-()\"#/@;:*<>{}`+=~|.!?,]", "", text)
    # print(text)
    return text
pool = ThreadPool(processes=12)

@router.get('/health-check')
def healthCheck():
    image = np.random.randint(255, size=(100,100,3),dtype=np.uint8)
    image_expanded = np.expand_dims(image, axis=0)
    with sess.as_default():
        sess.run(init_op)
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: image_expanded})
    return JSONResponse(content = {"message":"SUCCESS."}, status_code = 200)

@router.post('/predict')
async def predict_image(img: Item):
    try:
        uuid_gen = uuid.uuid4().hex
        date_time = datetime.datetime.now()
        print(date_time)
        image = base64.b64decode(img.img)
        npimg = np.frombuffer(image, np.uint8)
        image = cv2.imdecode(npimg,cv2.IMREAD_COLOR)
        rot_angle = rotate_angle(image)
        image = imutils.rotate(image, rot_angle)

        #Reshape from large image
        if  image.shape[0] > 3800 or image.shape[1] > 3800:
            image = cv2.resize(image, (int(image.shape[1]/3), int(image.shape[0]/3)))
            logging.info("after_reshape %s,%s" %(image.shape[0],image.shape[1]))

        elif  image.shape[0] > 2500 or image.shape[1] > 2500:
            image = cv2.resize(image, (int(image.shape[1]/2.5), int(image.shape[0]/2.5)))
            logging.info("after_reshape %s,%s" %(image.shape[0],image.shape[1]))

        elif  image.shape[0] > 1800 or image.shape[1] > 1800:
            image = cv2.resize(image, (int(image.shape[1]/1.5), int(image.shape[0]/1.5)))
            logging.info("after_reshape %s,%s" %(image.shape[0],image.shape[1]))

        image_copy = image.copy()
        im_height, im_width, channels = image.shape
        image_expanded = np.expand_dims(image, axis=0)
        imencoded = cv2.imencode('.jpg', image)[1].tobytes()
 
        with sess.as_default():
            sess.run(init_op)
            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: image_expanded})
            

        # Draw the results of the detection (aka 'visulaize the results')
        _,_,box_to_display_str_map = vis_util.visualize_boxes_and_labels_on_image_array(
                                                    image,
                                                    np.squeeze(boxes),
                                                    np.squeeze(classes).astype(np.int32),
                                                    np.squeeze(scores),
                                                    category_index,
                                                    use_normalized_coordinates=True,
                                                    line_thickness=1,
                                                    min_score_thresh=0.1)
        result = {
                    "idnumber":'',"enfirstname":'',"enlastname":'',"thname":'',
                    "thbirthdate":'',"enbirthdate":'',"religion":'',"address":'',
                    "thissuedate":'',"enissuedate":'',"thexpiredate":'',"enexpiredate":''
                    }

        worker = result.copy()

        for key,value in box_to_display_str_map.items():
            value = value[0].split(':')[0]
            if worker[value] == '':
                worker[value] = pool.apply_async(extract_text,(image_copy, key, value, im_height, im_width,))
                
        for k,v in worker.items():
            result[k]= v.get()

        return JSONResponse(content = result, status_code = 200)


    except Exception as e:
        print(e)
        return JSONResponse(content = {'message' : 'Error'}, status_code = 400)